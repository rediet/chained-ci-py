FROM python:3.9-alpine
LABEL maintainer="David Blaisonneau <david.blaisonneau@orange.com>"

ARG VCS_REF
ARG USER=user
ARG SRC=/usr/src/app/chained-ci-py

LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-py" \
      org.label-schema.description="chained-ci-py Docker image"

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ENV HOME /home/$USER

COPY chainedci $SRC/chainedci
COPY setup.* requirements.txt README.md MANIFEST.in $SRC/

WORKDIR $SRC
RUN adduser -D $USER &&\
      apk --no-cache --update-cache add --virtual build \
      build-base=~0.5 python3-dev=~3.9 py3-pip=~20.3 \
      libffi-dev=~3.4 openssl-dev=~1.1 && \
      apk --no-cache --update-cache add git=~2.34 && \
      pip install --no-cache-dir -r requirements.txt && \
      python setup.py build --executable /usr/local/bin/python &&\
      python setup.py install &&\
      rm -rf /usr/local/bin/ansible* && \
      apk del build &&\
      rm -rf /var/lib/apt/lists/* /tmp/* &&\
      rm -rf ~/.cache/pip

USER $USER
WORKDIR $HOME
CMD ["chainedci"]

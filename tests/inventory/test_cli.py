#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from chainedci.cli import main, generate
from chainedci.inventory import Inventory
import chainedci.config
import pytest
import mock
import sys
from tests.tests_lib import force_ini_values

force_ini_values()


class FakeOptions():
    opts = mock.mock.MagicMock()
    mode = 'run'
    key = None

    def __init__(self, mode='run'):
        mode = mock.mock.PropertyMock(return_value='run')
        type(self.opts).mode = self.mode


def _fake_getConfigIniShared(mode):
    fake_ini = chainedci.config.Config(load_env=False, init_scenarios=False)
    fake_ini['log'] = {'deprecation_warning': True}
    fake_ini['encryption'] = {'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}
    fake_ini['generator'] = {'ci_included_folder': '.gitlab-ci',
                             'ci_vault_input': 'file',
                             'ci_included_prefix': 'scenario_',
                             'gitlabci_main': '.gitlab-ci.test.yml',
                             'ci_dynamic_file': 'scenario.test.yml',
                             'mode': 'splitted',
                             'images': {'yaml_checking': 'yaml_checking'}}
    return fake_ini


def fake_getConfigIniSplitted(mode):
    fake_ini = _fake_getConfigIniShared(mode)
    fake_ini['generator']['mode'] = 'splitted'
    return fake_ini


def fake_getConfigIniDynamic(mode):
    fake_ini = _fake_getConfigIniShared(mode)
    fake_ini['generator']['mode'] = 'dynamic'
    return fake_ini


def fake_getConfigIniAllInOne(mode):
    fake_ini = _fake_getConfigIniShared(mode)
    fake_ini['generator']['mode'] = 'allinone'
    return fake_ini


def fake_getConfigRun(mode):
    return {'key': 'fake_password'}


def fake_getConfigRunNoKey(mode):
    return {'key': None}


def test_cli_main_run(mocker, caplog):
    mocker.patch('chainedci.cli.Inventory')
    run = mocker.patch('chainedci.cli.run')
    mocker.patch('chainedci.cli.Options', FakeOptions)
    mocker.patch.object(FakeOptions, 'mode', 'run')
    main()
    run.assert_called_once()
    assert "Start running the scenario job" in caplog.text


def test_cli_main_generate(mocker, caplog):
    mocker.patch('chainedci.cli.Inventory')
    generate = mocker.patch('chainedci.cli.generate')
    mocker.patch('chainedci.cli.Options', FakeOptions)
    mocker.patch.object(FakeOptions, 'mode', 'generate')
    main()
    generate.assert_called_once()
    assert "Start generating the ci file" in caplog.text


def test_cli_main_unknown(mocker, caplog):
    force_ini_values()
    mocker.patch('chainedci.cli.Inventory')
    mocker.patch('chainedci.cli.Options', FakeOptions)
    mocker.patch.object(FakeOptions, 'mode', 'badcommand')
    with pytest.raises(ValueError):
        main()
        assert 'Unknown command "badcommand"' in caplog.text


def test_generate_splitted(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRun)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniSplitted)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, None)
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_A.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_B.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_C.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_D.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci.test.yml', 'w', encoding='utf-8')


def test_generate_allinone(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRun)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniAllInOne)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, None)
    mocked_open.assert_any_call('.gitlab-ci.test.yml', 'w', encoding='utf-8')


def test_generate_dynamic_main(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRun)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniDynamic)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, None)
    mocked_open.assert_any_call('.gitlab-ci.test.yml', 'w', encoding='utf-8')


def test_generate_dynamic_scenario(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRun)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniDynamic)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, 'scenario_A')
    mocked_open.assert_any_call('scenario.test.yml', 'w', encoding='utf-8')


def test_generate_splitted_nokey(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRunNoKey)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniSplitted)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, None)
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_A.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_B.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_C.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci/scenario_scenario_D.yml', 'w', encoding='utf-8')
    mocked_open.assert_any_call('.gitlab-ci.test.yml', 'w', encoding='utf-8')


def test_generate_allinone_nokey(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRunNoKey)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniAllInOne)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, None)
    mocked_open.assert_any_call('.gitlab-ci.test.yml', 'w', encoding='utf-8')


def test_generate_dynamic_main_nokey(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRunNoKey)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniDynamic)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, None)
    mocked_open.assert_any_call('.gitlab-ci.test.yml', 'w', encoding='utf-8')


def test_generate_dynamic_scenario_nokey(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRunNoKey)
    inv = Inventory('tests/inventory/inventory/inventory')
    mocker.patch('chainedci.cli.getConfig', fake_getConfigIniDynamic)
    mocked_open = mocker.patch('chainedci.cli.open')
    generate(inv, 'scenario_A')
    mocked_open.assert_any_call('scenario.test.yml', 'w', encoding='utf-8')


def test_run(mocker):
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_C']
    sys.argv += ['-j', 'project1']
    sys.argv += ['run']
    inv = mocker.patch('chainedci.cli.Inventory')
    mocker.patch('chainedci.cli.Options', FakeOptions)
    mocker.patch.object(FakeOptions, 'mode', 'run')
    sc = mocker.patch('chainedci.cli.Scenario')
    main()
    inv.assert_called_once()
    sc.assert_called_once()


def test_init(mocker, caplog):
    sys.argv = ['chainedci.py']
    sys.argv += ['init']
    init = mocker.patch('chainedci.cli.init_project')
    main()
    init.assert_called_once()
    assert "Generate the " in caplog.text

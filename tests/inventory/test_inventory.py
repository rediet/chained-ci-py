#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import pytest
from ansible.parsing.vault import AnsibleVaultError
from ansible.inventory.manager import InventoryManager
from chainedci.inventory import Inventory
from chainedci.config import Config, getConfig
from chainedci.log import log
from tests.tests_lib import force_ini_values, rand_str, clean_secrets
from argparse import Namespace

clean_secrets()
force_ini_values()
log.setLevel('DEBUG')


def fake_getConfigRun(mode):
    if mode == 'run':
        return {'key': rand_str().encode("utf-8")}
    return Config(load_env=False)


def test_inventory_init(mocker):
    inventory_file = 'tests/inventory/inventory/inventory'
    inv = Inventory(inventory_file)
    assert isinstance(inv, Inventory)
    assert isinstance(inv.inventory, InventoryManager)


def test_inventory_full_load(caplog, mocker):
    mocker.patch('chainedci.inventory.add_tokens_to_log_filter')
    inventory_file = 'tests/inventory/inventory/inventory'
    scenario_name = 'scenario_A'
    job_name = 'project1'
    inv = Inventory(inventory_file)
    inv.load(scenario_name, job_name)
    assert "Load global group file " in caplog.text
    assert "Load group file " in caplog.text
    assert "Load scenario file " in caplog.text
    assert inv.job == 'project1'
    assert inv.scenario_name == 'scenario_A'
    assert inv.config['defaults']['gitlab']['pipeline']['delay'] == 15
    assert inv.config['defaults']['tokens'][0]['name'] == 'gitlab.com'
    assert inv.config['defaults']['specific_group_var'] == 'valueA'
    assert inv.config['scenarios']['scenario_A']['scenario_steps'][
        'project1']['project'] == 'project1'


def test_inventory_load_two_group(caplog, mocker):
    mocker.patch('chainedci.inventory.add_tokens_to_log_filter')
    inventory_file = 'tests/inventory/inventory/inventory'
    scenario_name = 'scenario_B'
    job_name = 'project1'
    inv = Inventory(inventory_file)
    inv.load(scenario_name, job_name)
    assert "No group file " in caplog.text
    assert "Load group file " in caplog.text


def test_inventory_load_no_group_file(caplog, mocker):
    mocker.patch('chainedci.inventory.add_tokens_to_log_filter')
    inventory_file = 'tests/inventory/inventory/inventory'
    scenario_name = 'scenario_C'
    job_name = 'project1'
    inv = Inventory(inventory_file)
    inv.load(scenario_name, job_name)
    assert "No group file " in caplog.text


def test_inventory_load_with_key(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRun)
    mocker.patch('chainedci.inventory.add_tokens_to_log_filter')
    inventory_file = 'tests/inventory/inventory/inventory'
    scenario_name = 'scenario_A'
    job_name = 'project1'
    inv = Inventory(inventory_file)
    inv.load(scenario_name, job_name)
    assert inv.job == 'project1'


def test_inventory_load_with_vaulted_file_but_no_key(mocker):
    mocker.patch('chainedci.inventory.add_tokens_to_log_filter')
    inventory_file = 'tests/inventory/inventory/inventory'
    scenario_name = 'scenario_D'
    job_name = 'project1'
    inv = Inventory(inventory_file)
    inv.load(scenario_name, job_name)
    assert "__VAULTED_VALUE__" in inv.config['scenarios'][
        'scenario_D']['scenario_steps']['project2']['trigger_token']


def test_inventory_scenarios_list(mocker):
    mocker.patch('chainedci.inventory.getConfig', fake_getConfigRun)
    mocker.patch('chainedci.inventory.add_tokens_to_log_filter')
    inventory_file = 'tests/inventory/inventory/inventory'
    inv = Inventory(inventory_file)
    assert list(inv.scenarios.keys()) == ['scenario_A', 'scenario_B',
                                          'scenario_D', 'scenario_C']

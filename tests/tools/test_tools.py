#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import logging
import pytest
import chainedci.version
from chainedci.config import config, Config, getConfig
from chainedci.log import log as logger, TokenFilter, add_tokens_to_log_filter
from chainedci.tools import (raise_ex, get_env_or_raise, token_selector,
                             waiting_cursor)
from tests.tests_lib import force_ini_values


def test_get_ini():
    ini = getConfig('ini')
    assert isinstance(ini, dict) is True


def test_get_ini_default_log_filename():
    ini = getConfig('ini')
    assert ini['log']['file']['name'] == 'chainedci.log'


def test_get_ini_user_log_level():
    ini = getConfig('ini')
    ini.merge('tests/tools/chainedci_test.yml')
    assert ini['log']['level'] == 'DEBUG'


def test_version():
    assert(isinstance(chainedci.version.__version__, str))


def test_raise_ex(caplog):
    force_ini_values()
    with pytest.raises(ValueError):
        raise_ex(ValueError, "my message")
    assert "my message" in caplog.text


def test_get_env_or_raise_OK():
    force_ini_values()
    ini = getConfig('ini')
    ini['env']['MYVAR'] = 'foobar'
    assert get_env_or_raise('MYVAR') == 'foobar'


def test_get_env_or_raise_Except(caplog):
    force_ini_values()
    with pytest.raises(ValueError):
        get_env_or_raise('UNKNOWNVAR')
    assert "Environmnent variable 'UNKNOWNVAR' MUST be set" in caplog.text


@pytest.mark.parametrize("url, res", [
    pytest.param('https://domain1', {'headername2': 'foo2'},
                 id='global_domain_token'),
    pytest.param('http://domain1', {'headername2': 'foo2'},
                 id='global_domain_token_without_http'),
    pytest.param('https://domain1/apiv2', {'headername2': 'foo2'},
                 id='sub_url_with_global_filter'),
    pytest.param('https://domain1/apiv1', {'headername1': 'foo1'},
                 id='sub_url_token'),
    pytest.param('https://domain2', {'headername3': 'bar'},
                 id='global_domain2_token'),
    pytest.param('https://domain3/api', {},
                 id='unknown_domain'),
])
def test_token_selector(caplog, url, res):
    config['defaults'] = {'tokens': [
        {'name': 'token1',
         'filter': 'https://domain1/apiv1',
         'id': 'headername1', 'value': 'foo1'},
        {'name': 'token2', 'filter': 'domain1',
         'id': 'headername2', 'value': 'foo2'},
        {'name': 'token3', 'filter': 'https://domain2',
         'id': 'headername3', 'value': 'bar'},
    ]}
    assert token_selector(url) == res


def test_token_selector_no_token(caplog):
    assert token_selector('xxx') == {}
    assert "token_selector - none found" in caplog.text


def test_token_selector_no_token_in_config(caplog):
    cfg_save = config['defaults']['tokens']
    del(config['defaults']['tokens'])
    assert token_selector('xxx') == {}
    assert "token_selector - none found" in caplog.text
    config['defaults']['tokens'] = cfg_save


def test_init_log(caplog):
    assert isinstance(logger, logging.Logger)
    logger.info('pytest')
    assert 'pytest' in caplog.text


def test_log_add_filters(caplog):
    logger.addFilter(TokenFilter(['aaa', 'bbb', b'ccc']))
    logger.info('pytest 1')
    assert 'pytest 1' in caplog.text
    logger.info('pytest 2 aaa')
    assert 'pytest 2 __HIDDEN__' in caplog.text
    logger.info('pytest 3 %(user)s', {'user': 'bbb'})
    assert 'pytest 3 __HIDDEN__' in caplog.text


def test_add_tokens_to_log_filter(caplog):
    cfg = Config()
    cfg['defaults'] = {'gitlab': {
        'git_projects': {
            'project1': {'trigger_token': 'xxxx'},
            'project2': {'trigger_token': 'yyyy'}
        }
    },
        'tokens': [
        {'name': 'token1',
         'filter': 'https://domain1/apiv1',
         'id': 'headername1', 'value': 'foo1'},
        {'name': 'token2', 'filter': 'domain1',
         'id': 'headername2', 'value': 'foo2'},
        {'name': 'token3', 'filter': 'https://domain2',
         'id': 'headername3', 'value': 'bar'},
    ]}
    add_tokens_to_log_filter(cfg, ['token_in_env'])
    logger.info('this is the value of token1: %s', 'foo1')
    assert 'this is the value of token1: __HIDDEN__' in caplog.text
    logger.info('this is the value of token3: %s', 'bar')
    assert 'this is the value of token3: __HIDDEN__' in caplog.text
    logger.info('this is the value of project1 trigger token: %s', 'xxxx')
    assert ('this is the value of project1 trigger token: __HIDDEN__'
            in caplog.text)
    logger.info('this is the value of TOKEN_IN_ENV: %s', 'token_in_env')
    assert 'this is the value of TOKEN_IN_ENV: __HIDDEN__' in caplog.text


def test_add_tokens_to_log_filter_empty_trigger_token(caplog):
    cfg = Config()
    cfg['defaults'] = {'gitlab': {
        'git_projects': {
            'project1': {'trigger_token': 'xxxx'},
            'project2': {'trigger_token': ''}
        }
    },
        'tokens': [
        {'name': 'token1',
         'filter': 'https://domain1/apiv1',
         'id': 'headername1', 'value': 'foo1'},
    ]}
    with pytest.raises(ValueError):
        add_tokens_to_log_filter(cfg)


def test_add_empty_tokens_to_log_filter(caplog):
    cfg = Config()
    cfg['defaults'] = {'tokens': [
        {'name': 'token1', 'filter': 'https://domain1/apiv1',
                           'id': 'headername1', 'value': ''}
    ]}
    with pytest.raises(ValueError):
        add_tokens_to_log_filter(cfg)


def test_disable_log_filter(caplog):
    ini = getConfig('ini')
    default_value = ini['log']['hide_tokens']
    ini['log']['hide_tokens'] = False
    add_tokens_to_log_filter(['token_in_env'])
    assert "NO filter added to the log system due" in caplog.text
    ini['log']['hide_tokens'] = default_value


def test_raise_ex_exception(caplog):
    ini = getConfig('ini')
    ini['log']['exception_or_exit'] = True
    with pytest.raises(ValueError):
        raise_ex(ValueError, "error message A")
    assert 'error message A' in caplog.text


def test_raise_ex_exit(caplog):
    ini = getConfig('ini')
    ini['log']['exception_or_exit'] = False
    with pytest.raises(SystemExit):
        raise_ex(ValueError, "error message B")
    assert 'error message B' in caplog.text


def test_waiting_cursor(caplog, mocker):
    ini = getConfig('ini')
    time_to_sleep = 15
    s = mocker.patch('chainedci.tools.sleep')
    ini['log']['level'] = 'INFO'
    waiting_cursor(time_to_sleep, end='\n')
    assert s.call_count == time_to_sleep


def test_waiting_cursor_debug(caplog, mocker):
    ini = getConfig('ini')
    time_to_sleep = 15
    s = mocker.patch('chainedci.tools.sleep')
    ini['log']['level'] = 'DEBUG'
    waiting_cursor(time_to_sleep, end='\n')
    assert s.call_count == 1

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import pytest
from chainedci.parser import JinjaParser

test_strings = [
  pytest.param("{{ lookup('env', 'env_var1') }}",
               "master",
               id="lookup1"),
  pytest.param("{{lookup('env','env_var1')}}",
               "master",
               id="lookup1_no_spaces"),
  pytest.param(" {{ lookup('env', 'env_var1') }} ",
               " master ",
               id="lookup1_space_in_string"),
  pytest.param("{{ lookup('env', 'env_var2') }}",
               "foo",
               id="lookup2"),
  pytest.param("{{ lookup('env', 'no_existing_var') }}",
               "",
               id="lookup_no_existing_var"),
  pytest.param("{{ lookup('env', 'no_existing_var')|default('bar')) }}",
               "bar",
               id="lookup_no_existing_var_with_default"),
  pytest.param("{{ lookup('env', 'no_existing_var')|default('bar', true)) }}",
               "bar",
               id="lookup_no_existing_var_with_default_true"),
  pytest.param("{{ lookup('env', 'no_existing_var')|default('bar')) }}",
               "bar",
               id="lookup_no_existing_var_with_default_spaces"),
  pytest.param("{{ lookup('env', 'env_var1')| default( 'bar' )) }}",
               "master",
               id="lookup1_with_default"),
  pytest.param("xx {{ lookup('env', 'env_var1')}} xx",
               "xx master xx",
               id="lookup1_string_around"),
  pytest.param("xx{{ lookup('env', 'env_var1')}}xx",
               "xxmasterxx",
               id="lookup1_string_around2"),
  pytest.param(True,
               True,
               id="non_string_data"),
]


@pytest.fixture
def mocked_ini():
    return {
        'env': {
            'env_var1': 'master',
            'env_var2': 'foo',
        }
    }


@pytest.mark.parametrize("data, resp", test_strings)
def test_artifact(data, resp, mocked_ini, mocker):
    mocker.patch('chainedci.parser.getConfig', lambda x: mocked_ini)
    parser = JinjaParser()
    assert resp == parser.parse(data)


def test_artifact_bad_syntax():
    with pytest.raises(ValueError):
        parser = JinjaParser()
        parser.parse("{{ bad_syntax }}")

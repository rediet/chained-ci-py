#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import argparse
import pytest
import sys
from chainedci.options import Options
from chainedci.config import getConfig
from chainedci.log import log
from tests.tests_lib import force_ini_values, rand_str, clean_secrets

force_ini_values()
log.setLevel('DEBUG')


def test_options_nominal_load(caplog):
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_D']
    sys.argv += ['-j', 'project1']
    sys.argv += ['run']
    opts = Options()
    assert opts.opts.inventory == 'tests/inventory/inventory/inventory'
    assert opts.opts.scenario_name == 'scenario_D'
    assert opts.opts.job == 'project1'


def test_options_load_with_vaulted_file_but_no_key(caplog):
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_D']
    sys.argv += ['-j', 'project1']
    sys.argv += ['run']
    opts = Options()
    assert "No vault password is set," in caplog.text


def test_options_load_with_env_key(caplog):
    force_ini_values()
    run = getConfig('run')
    run['key'] = ''
    ini = getConfig('ini')
    ini['env'][ini['encryption']['key_env_name']] = rand_str()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['-j', 'project1']
    sys.argv += ['run']
    opts = Options()
    assert run['key'] == ini['env'][ini['encryption']['key_env_name']].encode()
    assert "Vault password set with ENV VAR" in caplog.text
    del(ini['env'][ini['encryption']['key_env_name']])
    clean_secrets()


def test_options_load_with_key_file(caplog):
    force_ini_values()
    run = getConfig('run')
    run['key'] = ''
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_D']
    sys.argv += ['-j', 'project1']
    sys.argv += ['-p', 'tests/inventory/inventory/fake_vault_password']
    sys.argv += ['run']
    opts = Options()
    assert "Vault password set with file" in caplog.text
    assert run['key'] == b'fake_password'


def test_options_old_job_way(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['-e', 'step=project1']
    sys.argv += ['-p', 'tests/inventory/inventory/fake_vault_password']
    sys.argv += ['run']
    opts = Options()
    assert ("Please consider using option --job "
            "to replace '-e step=xxx' parameter") in caplog.text
    assert opts.opts.job == 'project1'


def test_options_load_with_bad_key_file(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['-j', 'project1']
    sys.argv += ['-p', 'tests/inventory/inventory/bad_vault_password_file']
    sys.argv += ['run']
    with pytest.raises(ValueError):
        Options()
        assert ("Vault password file 'tests/inventory/inventory"
                "/bad_vault_password_file' does not exists.") in caplog.text


def test_options_load_bad_inventory(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/bad_inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['-j', 'project1']
    sys.argv += ['-p', 'tests/inventory/inventory/fake_vault_password']
    sys.argv += ['run']
    with pytest.raises(ValueError):
        Options()
        assert ("Inventory file 'tests/inventory/inventory/bad_inventory'"
                "does not exists.") in caplog.text


def test_options_no_job(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['-p', 'tests/inventory/inventory/fake_vault_password']
    sys.argv += ['run']
    with pytest.raises(ValueError):
        Options()
        assert ("Please set --job parameter or "
                "--extra_vars parameter with 'step=xxxx' "
                "value") in caplog.text


def test_options_bad_command(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['-p', 'tests/inventory/inventory/fake_vault_password']
    sys.argv += ['badcommand']
    with pytest.raises(SystemExit):
        opts = Options()


def test_options_run_missing_job(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-s', 'scenario_A']
    sys.argv += ['run']
    with pytest.raises(ValueError):
        opts = Options()


def test_options_run_missing_scenario(caplog):
    force_ini_values()
    sys.argv = ['chainedci.py']
    sys.argv += ['-i', 'tests/inventory/inventory/inventory']
    sys.argv += ['-j', 'project1']
    sys.argv += ['run']
    with pytest.raises(ValueError):
        opts = Options()

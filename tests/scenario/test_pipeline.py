#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import mock
import pytest
import random
import time
from chainedci.pipeline import Pipeline
from tests.tests_lib import rand_str
from chainedci.config import getConfig, config
from chainedci.log import log
from tests.tests_lib import force_ini_values

force_ini_values()
ini = getConfig('ini')
log.setLevel('DEBUG')

config['defaults'] = {'tokens': [
    {'name': 'token', 'filter': 'https://good.gitlab',
     'id': 'PRIVATE-TOKEN', 'value': 'xxxxx'},
]}


@pytest.fixture
def params():
    return {rand_str(): rand_str() for i in range(0, random.randint(1, 5))}


def prepared_pipeline(params, status='pending',
                      url="https://good.gitlab/api/v4/projects/42"):
    token = rand_str()
    ref = rand_str()
    p = Pipeline(url, token, params, ref)
    ini = getConfig('ini')
    ini['env'][ini['auth']['private_token_env']] = 'fake_token'
    p.pipeline_id = 666
    p.status = {'status': status,
                'start': 1,
                'timeout': 15,
                'step_running_timeout': None,
                'last_response': {'id': 666, 'created_at': 'today',
                                  'status': status, 'web_url': 'foo.bar'}}
    p.stop = status in ['success', 'failed']
    return (url, p.pipeline_id, p)


@pytest.mark.parametrize('execution_number', range(5))
def test_pipeline(params, execution_number):
    url = "https://good.gitlab/api/v4/projects/42"
    token = rand_str()
    ref = rand_str()
    p = Pipeline(url, token, params, ref)
    assert isinstance(p, Pipeline)
    assert p.base_url == url
    for k, v in params.items():
        assert p.parameters[f'variables[{k}]'] == v
    assert p.parameters['ref'] == ref
    assert p.parameters['token'] == token


@pytest.mark.parametrize('execution_number', range(3))
def test_start(params, execution_number, requests_mock, caplog):
    force_ini_values()
    url = "https://good.gitlab/api/v4/projects/42"
    token = rand_str()
    ref = rand_str()
    p = Pipeline(url, token, params, ref)
    resp_data = {'id': 666, 'created_at': 'today',
                 'status': 'pending', 'web_url': 'foo.bar'}
    requests_mock.post(url+"/trigger/pipeline",
                       json=resp_data,
                       status_code=201
                       )
    p.start()
    assert p.status['last_response'] == resp_data
    assert p.pipeline_id == resp_data['id']
    assert isinstance(p.status['start'], float)
    assert isinstance(p.status['timeout'], float)
    assert p.status['status'] == resp_data['status']
    assert "Pipeline: start - Pipeline created" in caplog.text


@pytest.mark.parametrize('status_code', [401, 404, 500])
def test_start_fail(params, status_code, requests_mock):
    url = "https://bad.gitlab/api/v4/projects/0"
    token = rand_str()
    ref = rand_str()
    p = Pipeline(url, token, params, ref)
    requests_mock.post(url+"/trigger/pipeline",
                       status_code=status_code)
    with pytest.raises(ValueError):
        p.start()


def test_status_fail_bad_status_code(params, requests_mock, caplog):
    (url, pid, p) = prepared_pipeline(params)
    requests_mock.get(f"{url}/pipelines/{pid}",
                      json={'error': 'fake'}, status_code=500)
    with pytest.raises(ValueError):
        p.get_remote_pipeline_status()
    assert "Pipeline: run - Failed to get status at" in caplog.text


@pytest.mark.parametrize('status_code', [200, 401])
def test_status_empty_answer(params, requests_mock, status_code, caplog):
    ini = getConfig('ini')
    (url, pid, p) = prepared_pipeline(params)
    if status_code == 200:
        requests_mock.get(f"{url}/pipelines/{pid}", text='', status_code=200)
    else:
        requests_mock.get(f"{url}/pipelines/{pid}",
                          json="{'error': 'unauthorized'}", status_code=401)
    p.get_remote_pipeline_status()
    assert isinstance(p.status['start'], float)
    assert isinstance(p.status['timeout'], float)
    assert p.status['timeout'] == p.status['start'] + ini['timers']['unknown']
    assert p.status['status'] == 'unknown'
    assert ("Pipeline: status changed from 'pending' to "
            "'unknown'") in caplog.text


@pytest.mark.parametrize('status', ['success', 'failed', 'canceled'])
def test_status_stop_status(params, requests_mock, status, caplog):
    (url, pid, p) = prepared_pipeline(params)
    requests_mock.get(f"{url}/pipelines/{pid}",
                      json={'status': status}, status_code=200)
    assert p.stop is False
    p.get_remote_pipeline_status()
    assert p.status['status'] == status
    assert p.stop is True
    assert f"Pipeline: stopping, status changed to '{status}'" in caplog.text


def test_status_stop_status_INFO(params, requests_mock, caplog):
    ini = getConfig('ini')
    ini['log']['level'] = 'INFO'
    (url, pid, p) = prepared_pipeline(params)
    requests_mock.get(f"{url}/pipelines/{pid}",
                      json={'status': 'success'}, status_code=200)
    assert p.stop is False
    p.get_remote_pipeline_status()
    assert p.status['status'] == 'success'
    assert p.stop is True
    assert f"Pipeline: stopping, status changed to 'success'" in caplog.text


def test_status_pending_to_running(params, requests_mock, caplog):
    ini = getConfig('ini')
    (url, pid, p) = prepared_pipeline(params)
    resp_data = {'status': 'running'}
    requests_mock.get(f"{url}/pipelines/{pid}",
                      json=resp_data, status_code=200)
    p.get_remote_pipeline_status()
    assert p.status['last_response'] == resp_data
    assert isinstance(p.status['start'], float)
    assert isinstance(p.status['timeout'], float)
    assert p.status['timeout'] == p.status['start'] + ini['timers']['running']
    assert p.status['status'] == 'running'
    assert "Pipeline: status changed" in caplog.text


def test_status_unknown_to_running(params, requests_mock, caplog):
    ini = getConfig('ini')
    (url, pid, p) = prepared_pipeline(params)
    p.status['status'] = 'unknown'
    p.status['previous_time'] = 3.0
    resp_data = {'status': 'running'}
    requests_mock.get(f"{url}/pipelines/{pid}",
                      json=resp_data, status_code=200)
    p.get_remote_pipeline_status()
    assert p.status['last_response'] == resp_data
    assert isinstance(p.status['start'], float)
    assert isinstance(p.status['timeout'], float)
    assert p.status['start'] == 3.0
    assert p.status['timeout'] == (p.status['previous_time']
                                   + ini['timers']['running'])
    assert p.status['status'] == 'running'
    assert ("Pipeline: status changed from 'unknown' to "
            "'running'") in caplog.text


@pytest.mark.skipif(ini['env'].eval('SKIP_LONG_TEST'),
                    reason="skipping long tests (SKIP_LONG_TEST var)")
@pytest.mark.parametrize('sleep, retries',
                         [(random.randrange(1, 5)/10, random.randrange(1, 5)),
                          (random.randrange(1, 5)/10, random.randrange(5, 10)),
                          (random.randrange(1, 5)/20, random.randrange(10, 30))
                          ])
def test_wait_for_remote_pipeline_timeout(params, mocker,
                                          sleep, retries, caplog):
    force_ini_values()
    ini = getConfig('ini')
    ini['log']['level'] = 'DEBUG'
    ini['timers']['retries_sleep'] = sleep
    (url, pid, p) = prepared_pipeline(params)
    p.status['start'] = time.time()
    p.status['timeout'] = p.status['start'] + sleep*retries
    mocker.spy(p, 'get_remote_pipeline_status')
    p.get_remote_pipeline_status = mocker.MagicMock(return_value=None)
    with pytest.raises(TimeoutError):
        p.wait_for_remote_pipeline()
    assert p.get_remote_pipeline_status.call_count == retries
    assert "Pipeline: Timeout, pipeline" in caplog.text


def test_set_custom_timeout(params):
    force_ini_values()
    ini = getConfig('ini')
    ini['timers']['retries_sleep'] = 1
    ini['timers']['timeout_retrocompatibility'] = False
    (url, pid, p) = prepared_pipeline(params)
    p.set_custom_timeout(42)
    assert p.status['step_running_timeout'] == 42


def test_set_custom_timeout_old_way(params):
    force_ini_values()
    ini = getConfig('ini')
    ini['timers']['retries_sleep'] = 10
    ini['timers']['timeout_retrocompatibility'] = True
    (url, pid, p) = prepared_pipeline(params)
    p.set_custom_timeout(42)
    assert p.status['step_running_timeout'] == 420
    p._set_status('running', 100)
    assert p.status['start'] == 100
    assert p.status['timeout'] == 520
    assert p.status['status'] == 'running'



def test_custom_timeout(params):
    ini = getConfig('ini')
    ini['timers']['timeout_retrocompatibility'] = False
    (url, pid, p) = prepared_pipeline(params)
    p.status['step_running_timeout'] = 42
    p._set_status('running', 100)
    assert p.status['start'] == 100
    assert p.status['timeout'] == 142
    assert p.status['status'] == 'running'


@pytest.mark.skipif(ini['env'].eval('SKIP_LONG_TEST'),
                    reason="skipping long tests (SKIP_LONG_TEST var)")
@pytest.mark.parametrize('sleep, retries',
                         [(random.randrange(1, 5)/10, random.randrange(1, 5)),
                          (random.randrange(1, 5)/10, random.randrange(5, 10)),
                          (random.randrange(1, 5)/20, random.randrange(10, 30))
                          ])
def test_wait_for_remote_pipeline_stop(params, mocker,
                                       sleep, retries, caplog):
    force_ini_values()
    ini = getConfig('ini')
    ini['log']['level'] = 'DEBUG'
    ini['timers']['retries_sleep'] = sleep

    def mocked_get_remote_pipeline_status(self):
        self.stop = self.get_remote_pipeline_status.call_count > (retries)
    mocker.patch("chainedci.pipeline.Pipeline.get_remote_pipeline_status",
                 mocked_get_remote_pipeline_status)
    (url, pid, p) = prepared_pipeline(params)
    p.status['start'] = time.time()
    p.status['timeout'] = p.status['start'] + sleep*(retries + 2)
    mocker.spy(p, 'get_remote_pipeline_status')
    ini['timers']['retries_sleep'] = sleep
    p.wait_for_remote_pipeline()
    assert p.get_remote_pipeline_status.call_count == retries + 1
    assert "Pipeline - Loop ended" in caplog.text


@mock.patch('chainedci.pipeline.Pipeline.start')
@mock.patch('chainedci.pipeline.Pipeline.wait_for_remote_pipeline')
def test_loop(mocked_start, mocked_wait):
    p = Pipeline(rand_str(), rand_str(), {}, rand_str())
    p.loop()
    mocked_start.assert_called_once()
    mocked_wait.assert_called_once()


def test_artifact_url_failed_job_not_successful(params, caplog):
    (url, pid, p) = prepared_pipeline(params, status='failed')
    with pytest.raises(ValueError):
        p.artifact_url('job_name')
    assert ("Pipeline - no artifact will be available with that "
            "'failed' pipeline") in caplog.text


def test_artifact_url_failed_get_not_200(params, requests_mock, caplog):
    (url, pid, p) = prepared_pipeline(params, status='success')
    jobs_list_url = f"{url}/pipelines/{pid}/jobs/?scope[]=success"
    requests_mock.get(jobs_list_url,
                      json={"error": "error message"}, status_code=500)
    with pytest.raises(ValueError):
        p.artifact_url('job_name')
    assert ("Pipeline: artifact_url - Failed to get jobs at "
            f"'{jobs_list_url}' with code [500]") in caplog.text


@pytest.fixture
def jobs_list_resp():
    artif_file = {'filename': 'aaa', 'size': 666}
    return [
        {'name': 'job1', 'status': 'success', 'id': 1},
        {'name': 'job2', 'status': 'success', 'id': 2,
         'artifacts_file': artif_file},
        {'name': 'job3', 'status': 'success', 'id': 3,
         'artifacts_file': artif_file},
        {'name': 'job3', 'status': 'success', 'id': 4,
         'artifacts_file': artif_file},
    ]


def test_artifact_url_failed_not_enough_jobs(params, requests_mock,
                                             jobs_list_resp, caplog):
    (url, pid, p) = prepared_pipeline(params, status='success')
    jobs_list_url = f"{url}/pipelines/{pid}/jobs/?scope[]=success"
    requests_mock.get(jobs_list_url,
                      json=jobs_list_resp, status_code=200)
    with pytest.raises(ValueError):
        p.artifact_url('job_nameX')
    assert ("Pipeline: artifact_url - Failed to get the jobs list "
            "with name 'job_nameX'") in caplog.text


def test_artifact_url_failed_no_file(params, requests_mock,
                                     jobs_list_resp, caplog):
    (url, pid, p) = prepared_pipeline(params, status='success')
    jobs_list_url = f"{url}/pipelines/{pid}/jobs/?scope[]=success"
    requests_mock.get(jobs_list_url,
                      json=jobs_list_resp, status_code=200)
    with pytest.raises(ValueError):
        p.artifact_url('job1')
    assert ("Pipeline: artifact_url - No artifact file linked "
            f"with this job ({jobs_list_resp[0]})") in caplog.text


@pytest.mark.parametrize("job_name, job_list_id",
                         [pytest.param('job2', 1, id='no_duplicate_job'),
                          pytest.param('job3', 3, id='retried_job')])
def test_artifact_url_success_with_retry(params, requests_mock,
                                         jobs_list_resp, caplog,
                                         job_name, job_list_id):
    (url, pid, p) = prepared_pipeline(params, status='success')
    jobs_list_url = f"{url}/pipelines/{pid}/jobs/?scope[]=success"
    requests_mock.get(jobs_list_url,
                      json=jobs_list_resp, status_code=200)
    artif_url = p.artifact_url(job_name)
    assert artif_url == (f"{url}/jobs/"
                         f"{jobs_list_resp[job_list_id]['id']}/artifacts")
    assert f"Pipeline - artifact is here '{artif_url}'" in caplog.text

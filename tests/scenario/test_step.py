#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import mock
import pytest
from chainedci.config import Config, getConfig
from chainedci.env_vars import EnvVars
from chainedci.step import Step
from chainedci.log import log
from tests.tests_lib import force_ini_values

force_ini_values()
ini = getConfig('ini')
log.setLevel('DEBUG')

steps_config = [
    pytest.param({'stage': 'config',
                  'url': 'https://gitlab.me/project',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'config',
                  'infra': 'None'},
                 (None, None),
                 id='step_no_artifact'),
    pytest.param({'stage': 'config',
                  'url': 'https://url/project',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'config'},
                 ({'encrypt': False,
                   'infra_pdfidf': 'fooSc'}, None),
                 id='step_basic_config'),
    pytest.param({'stage': 'config',
                  'url': 'https://url/project',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'config',
                  'certificates': 'certificate.eyml',
                  'ssh_access': 'ssh_access.eyml'},
                 ({'certificates': 'certificate.eyml',
                   'ssh_access': 'ssh_access.eyml',
                   'encrypt': False,
                   'infra_pdfidf': 'fooSc'}, None),
                 id='step_basic_with_certs'),
    pytest.param({'stage': 'config',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'config',
                  'infra': 'another'},
                 ({'encrypt': False,
                   'infra_pdfidf': 'another'}, None),
                 id='step_local_pdfidf'),
    pytest.param({'stage': 'config',
                  'api': 'https://api',
                  'path': 'specific_path',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'config'},
                 ({'encrypt': False,
                   'infra_pdfidf': 'fooSc'}, None),
                 id='step_pdfidf_by_api'),
]
steps_pipeline = [
    pytest.param({'stage': 'apps',
                  'branch': "{{ lookup('env','trigger_branch')|"
                            "default('master', true) }}",
                  'project': 'trigger',
                  'trigger_token': 'xxxxxxx'},
                 (None, None),
                 id='step_trigger_myself'),
    pytest.param({'url': 'https://gitlab.me/project2',
                  'api': 'https://gitlab.me/api/v4/projects/666',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'project1',
                  'novault': ['file1', 'file2'],
                  'extra_parameters': {'param1': 'value1'},
                  'parameters': {'VAR1': "{{ lookup('env','var1')|"
                                         "default('x') }}",
                                 'VAR2': 'foo',
                                 'VAR3': 'bar'},
                  'stage': 'stage2',
                  'trigger_token': 'xxxxxxx',
                  },
                 (None, {'VAR1': 'x',
                         'VAR2': 'foo',
                         'VAR3': 'bar',
                         'param1': 'value1',
                         'NOVAULT_LIST': 'file1\\nfile2'}),
                 id='step_with_parameters'),
    pytest.param({'url': 'https://gitlab.me/project2',
                  'api': 'https://gitlab.me/api/v4/projects/666',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'project1',
                  'get_artifacts': 'config',
                  'get_bin': True,
                  'get_encrypt': True,
                  'pull_artifacts': 'install:triggered',
                  'stage': 'stage2',
                  'trigger_token': 'xxxxxxx',
                  'certificates': 'certificate.eyml',
                  'ssh_access': 'ssh_access.eyml'
                  },
                 ({'get_artifacts': 'config',
                   'certificates': 'certificate.eyml',
                   'ssh_access': 'ssh_access.eyml',
                   'encrypt': True}, None),
                 id='step_with_get_artifact'),
    pytest.param({'url': 'https://gitlab.me/project2',
                  'api': 'https://gitlab.me/api/v4/projects/666',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'project1',
                  'local_files': [{'var/data.yml': 'data.yml'}],
                  'stage': 'stage2',
                  'trigger_token': 'xxxxxxx'
                  },
                 ({'local_files': [{'var/data.yml': 'data.yml'}],
                   'encrypt': False}, None),
                 id='step_with_local_files'),
    pytest.param({'url': 'https://gitlab.me/project2',
                  'api': 'https://gitlab.me/api/v4/projects/666',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'project1',
                  'local_files': [{'var/data.yml': 'data.yml'}],
                  'stage': 'stage2',
                  'trigger_token': 'xxxxxxx',
                  'timeout': 42
                  },
                 ({'local_files': [{'var/data.yml': 'data.yml'}],
                   'encrypt': False}, None),
                 id='step_with_timeout'),
    pytest.param({'url': 'https://gitlab.me/project2',
                  'api': 'https://gitlab.me/api/v4/projects/666',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'project': 'project1',
                  'remote_files': [{'url1': 'data.yml'}],
                  'stage': 'stage2',
                  'trigger_token': 'xxxxxxx'
                  },
                 ({'remote_files': [{'url1': 'data.yml'}],
                   'encrypt': False}, None),
                 id='step_with_remote_files'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'get_artifacts': 'config',
                  'project': 'project3',
                  'stage': 'check',
                  'certificates': 'certificate.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'certificates': 'certificate.eyml',
                   'encrypt': False}, None),
                 id='step_with_certificate_old_way'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config', },
                  'project': 'project3',
                  'stage': 'check',
                  'group_certificates': 'certificate_global.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'certificates': 'certificate_global.eyml',
                   'encrypt': False}, None),
                 id='step_with_global_certificate_and_none_in_input_artifact'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config',
                      'certificates': 'certificate.eyml'},
                  'project': 'project3',
                  'stage': 'check',
                  'group_certificates': 'certificate_global.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'certificates': 'certificate.eyml',
                   'encrypt': False}, None),
                 id='step_with_global_and_input_artifact_certificate'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'get_artifacts': 'config',
                  'project': 'project3',
                  'stage': 'check',
                  'ansible_ssh_creds': 'ssh_creds.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'ssh_creds': 'ssh_creds.eyml',
                   'encrypt': False}, None),
                 id='step_with_ssh_creds_old_way'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config', },
                  'project': 'project3',
                  'stage': 'check',
                  'group_ssh_creds': 'ssh_creds_global.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'ssh_creds': 'ssh_creds_global.eyml',
                   'encrypt': False}, None),
                 id='step_with_global_ssh_creds_and_none_in_input_artifact'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config',
                      'ssh_creds': 'ssh_creds.eyml'},
                  'project': 'project3',
                  'stage': 'check',
                  'group_ssh_creds': 'ssh_creds_global.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'ssh_creds': 'ssh_creds.eyml',
                   'encrypt': False}, None),
                 id='step_with_global_and_input_artifact_ssh_creds'),

    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'get_artifacts': 'config',
                  'project': 'project3',
                  'stage': 'check',
                  'ssh_access': 'ssh_access.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'ssh_access': 'ssh_access.eyml',
                   'encrypt': False}, None),
                 id='step_with_ssh_access_old_way'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config', },
                  'project': 'project3',
                  'stage': 'check',
                  'group_ssh_access': 'ssh_access_global.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'ssh_access': 'ssh_access_global.eyml',
                   'encrypt': False}, None),
                 id='step_with_global_ssh_access_and_none_in_input_artifact'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config',
                      'ssh_access': 'ssh_access.eyml'},
                  'project': 'project3',
                  'stage': 'check',
                  'group_ssh_access': 'ssh_access_global.eyml',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'ssh_access': 'ssh_access.eyml',
                   'encrypt': False}, None),
                 id='step_with_global_and_input_artifact_ssh_access'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'branch': "{{ lookup('env','config_branch')|"
                            "default('master', true) }}",
                  'input_artifact': {
                      'get_artifacts': 'config',
                      'certificates': 'certificate.eyml',
                      'ssh_access': 'ssh_access.eyml'},
                  'project': 'project3',
                  'stage': 'check',
                  'trigger_token': 'xxxxxxx',
                  },
                 ({'get_artifacts': 'config',
                   'certificates': 'certificate.eyml',
                   'ssh_access': 'ssh_access.eyml',
                   'encrypt': False}, None),
                 id='step_with_artifact_new_way'),
    pytest.param({'url': 'https://url',
                  'api': 'https://api',
                  'project': 'project3',
                  'stage': 'check',
                  'trigger_token': 'xxxxxxx',
                  'ansible_ssh_creds': 'ssh_creds.eyml',
                  },
                 ({'ssh_creds': 'ssh_creds.eyml',
                   'encrypt': False}, None),
                 id='step_with_ssh_creds')
]


fake_param_data = [
    ({}, {}),
    ({'a': 1}, {'variables[a]': 1}),
    ({'a': 1, 'b': 2}, {'variables[a]': 1, 'variables[b]': 2}),
    ({'a': 'aaa', 'b': 'bbb'}, {'variables[a]': 'aaa', 'variables[b]': 'bbb'})
]


class MockArtifact(dict):
    def __init__(self, data, name, config_source):
        self.update(data)

    @property
    def bin(self):
        return b'foobar'

    def get_local(self, folder='local_folder'):
        log.info("MockArtifact: get_local to %s", folder)


class MockPipeline():
    def __init__(self, base_url, token, parameters, ref):
        self.base_url = base_url
        self.parameters = {f'variables[{k}]': v for k, v in parameters.items()}
        self.parameters['token'] = token
        self.parameters['ref'] = ref
        self.status = {'status': 'status'}
        self.timeout = None

    def artifact_url(self, step_name):
        log.info(f"MockPipeline: get artifact url for {step_name}")
        return f"https://gitlab.com/{step_name}"

    def start(self):
        log.info("MockPipeline: start")

    def loop(self):
        log.info("MockPipeline: loop")

    def set_custom_timeout(self, timeout):
        self.timeout = timeout
        log.info("MockPipeline: set_custom_timeout")


def extra_params(name):
    extra_src = {'pod': 'pod_name', 'jumphost': 'jumpserver'}
    source_job_name = {'source_job_name': name}
    extra_ret = {**extra_src, **source_job_name}
    return (extra_src, extra_ret)


def fake_getConfig(cfg):
    if cfg == 'run':
        fake_run = Config(load_env=False, init_scenarios=False)
        fake_run['name'] = 'scenarioName'
        return fake_run
    elif cfg == 'ini':
        fake_ini = Config(load_env=False, init_scenarios=False)
        fake_ini['log'] = {'deprecation_warning': True}
        fake_ini['env'] = EnvVars(load=False)
        fake_ini['env']['CI_JOB_NAME'] = 'FakeCI'
        fake_ini['env']['CI_PROJECT_ID'] = '666'
        fake_ini['specific_steps'] = {'config': 'config',
                                      'trigger_myself': 'trigger'}

        return fake_ini
    return getConfig('chainedci')


fake_config = {'scenarioName': {'scenario_steps': []}}


@pytest.mark.parametrize("step_data, artifact_result", steps_config)
def test_step_config(step_data, artifact_result, mocker):
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    name = step_data['project']
    extra = extra_params(name)
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    s = Step(name, step_data, extra[0], 'fooSc')
    assert isinstance(s, dict) is True
    # Check artifact
    if artifact_result[0]:
        assert s.artifact == artifact_result[0]
    # Check parameters
    if artifact_result[1]:
        assert s.parameters == {**artifact_result[1], **extra[1]}
    else:
        assert s.parameters == extra[1]


@pytest.mark.parametrize("step_data, artifact_result", steps_pipeline)
def test_step_pipeline(step_data, artifact_result, mocker):
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    name = step_data['project']
    extra = extra_params(name)
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    s = Step(name, step_data, extra[0], 'fooSc')
    assert isinstance(s, dict) is True
    # Check artifact
    if artifact_result[0]:
        assert s.artifact == artifact_result[0]
    # Check parameters
    if artifact_result[1]:
        assert s.parameters == {**artifact_result[1], **extra[1]}
    else:
        assert s.parameters == extra[1]


@pytest.mark.parametrize("step_data, artifact_result", [steps_config[0]])
def test_deprecation_log(step_data, artifact_result, mocker, caplog):
    extra = extra_params('config')
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    Step('config', step_data, extra[0], 'fooSc')
    assert "DEPRECATION: please update step" in caplog.text


@pytest.mark.parametrize("step_data, artifact_result", [steps_config[1]])
def test_config_source_url(step_data, artifact_result, mocker, caplog):
    config = {
        'defaults': {
            'disable_pages': True,
            'gitlab': {
                'api_url': 'https://gitlab.com/api/v4',
                'base_url': 'https://gitlab.com',
                'git_projects': {
                    'config': {
                        'branch': 'master',
                        'path': 'tp_config',
                        'stage': 'config',
                        'url': 'https://gitlab.com/config_project'},
                    'project': {
                        'api': 'https://gitlab.com/api/v4/projects/123456789',
                        'branch': 'master',
                        'timeout': 300,
                        'trigger_token': '123456789_token',
                        'url': 'https://gitlab.com/'},
                }
            }
        },
        'scenarioName': {'jumphost': None,
                         'scenario_steps': {
                             'config': {'project': 'config',
                                        'pull_artifacts': 'job2'},
                             'project': {'get_artifacts': 'config',
                                         'project': 'project'}}}
    }
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    extra = extra_params('config')
    mocker.patch('chainedci.step.config', config)
    artif = mocker.patch('chainedci.step.Artifact', mocker.MagicMock())
    Step('config', step_data, extra[0], 'fooSc')
    print(artif.__dict__)
    artif.assert_called_once_with({'infra_pdfidf': 'fooSc', 'encrypt': False},
                                  'config',
                                  {'url': 'https://gitlab.com/config_project',
                                   'branch': 'master',
                                   'path': 'tp_config'})


@pytest.mark.parametrize("step_data, artifact_result", [steps_config[1]])
def test_config_source_api(step_data, artifact_result, mocker, caplog):
    config = {
        'defaults': {
            'disable_pages': True,
            'gitlab': {
                'api_url': 'https://gitlab.com/api/v4',
                'base_url': 'https://gitlab.com',
                'git_projects': {
                    'config': {
                        'branch': 'master',
                        'path': 'tp_config',
                        'stage': 'config',
                        'api': 'https://gitlab.com/api/v4/projects/987654321'},
                    'project': {
                        'api': 'https://gitlab.com/api/v4/projects/123456789',
                        'branch': 'master',
                        'timeout': 300,
                        'trigger_token': '123456789_token',
                        'url': 'https://gitlab.com/'},
                }
            }
        },
        'scenarioName': {'jumphost': None,
                         'scenario_steps': {
                             'config': {'project': 'config',
                                        'pull_artifacts': 'job2'},
                             'project': {'get_artifacts': 'config',
                                         'project': 'project'}}}
    }
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    extra = extra_params('config')
    mocker.patch('chainedci.step.config', config)
    artif = mocker.patch('chainedci.step.Artifact', mocker.MagicMock())
    Step('config', step_data, extra[0], 'fooSc')
    print(artif.__dict__)
    artif.assert_called_once_with({'infra_pdfidf': 'fooSc', 'encrypt': False},
                                  'config',
                                  {'api': 'https://gitlab.com/api/v4/projects/'
                                          '987654321',
                                   'branch': 'master',
                                   'path': 'tp_config'})


@pytest.mark.parametrize("step_data, artifact_result", steps_pipeline[1:])
def test_step_run_with_remote_pipeline(step_data, artifact_result, caplog,
                                       mocker, requests_mock):
    extra = extra_params('remote_pipeline')
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    mocker.patch('chainedci.step.Pipeline', MockPipeline)
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    s = Step('step0', step_data, extra[0], 'fooSc')
    s._pull_artifact = mocker.MagicMock()
    step_run_result = s.run()
    step_run_result == s.pipeline.status['status']
    assert s.pipeline.base_url == step_data['api']
    assert s.pipeline.parameters['ref'] == 'master'
    assert s.pipeline.parameters['token'] == 'xxxxxxx'
    assert s.pipeline.parameters['variables[source_job_name]'] == 'step0'
    assert "MockPipeline: loop" in caplog.text
    if 'pull_artifacts' in step_data:
        assert s._pull_artifact.call_count == 1


@pytest.mark.parametrize("step_data, artifact_result", [steps_config[0]])
def test_step_run_local_without_artifact(step_data, artifact_result, caplog,
                                         mocker):
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    extra = extra_params('config')
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    s = Step('config', step_data, extra[0], 'fooSc')
    ini['env'][ini['auth']['private_token_env']] = 'fake_token'
    s.run()
    assert "run - no artifact, nothing to do" in caplog.text


@pytest.mark.parametrize("step_data, artifact_result", [steps_config[1]])
def test_step_run_local_with_artifact(step_data, artifact_result, caplog,
                                      mocker):
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    extra = extra_params('config')
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    s = Step('config', step_data, extra[0], 'fooSc')
    ini['env'][ini['auth']['private_token_env']] = 'fake_token'
    s.run()
    assert "MockArtifact: get_local" in caplog.text


@pytest.mark.parametrize("step_data, artifact_result", [steps_pipeline[0]])
def test_step_run_trigger_myself(step_data, artifact_result, caplog,
                                 mocker, requests_mock):
    extra = extra_params('config')
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    mocker.patch('chainedci.step.Pipeline', MockPipeline)
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    fake_config = Config(load_env=False, init_scenarios=False)
    fake_config['gitlab'] = {'api_url': 'https://fake.gitlab/api/v4'}
    mocker.patch('chainedci.step.config', fake_config)
    s = Step('trigger', step_data, extra[0], 'fooSc')
    s._pull_artifact = mocker.MagicMock()
    s.run()
    assert s.pipeline.base_url == ('https://fake.gitlab/api/v4/projects'
                                   '/666/trigger/pipeline')
    assert s.pipeline.parameters['ref'] == 'master'
    assert s.pipeline.parameters['token'] == 'xxxxxxx'
    assert s.pipeline.parameters['variables[source_job_name]'] == 'trigger'
    assert 'MockPipeline: start' in caplog.text


@mock.patch('chainedci.step.ArtifactSrc')
@pytest.mark.parametrize("step_data, artifact_result", [steps_pipeline[2]])
def test_step_pull_artifact(artif_src_mock, step_data,
                            artifact_result, mocker):
    extra = extra_params('config')
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    s = Step('foo', step_data, extra[0], 'fooSc')
    s.pipeline = mocker.MagicMock()
    s.pipeline.artifact_url = mocker.MagicMock(
        side_effect=['https://foo.bar/artifact.zip'])
    s._pull_artifact()
    s.pipeline.artifact_url.assert_called_once()
    artif_src_mock.assert_called_once()


steps_data_only_except = [
    pytest.param({'only': ["VAR"]}, True,
                 {"VAR": "VAL"},
                 id='step_only_one_run'),
    pytest.param({'only': ["VAR", "VAR2 == foo"]}, True,
                 {"VAR": "VAL", "VAR2": "foo"},
                 id='step_only_two_run'),
    pytest.param({'only': ["VAR"]}, False,
                 {},
                 id='step_only_one_skip'),
    pytest.param({'only': ["VAR", "VAR2 == foo"]}, False,
                 {"VAR": "VAL"},
                 id='step_only_two_skip'),
    pytest.param({'except': ["VAR"]}, True,
                 {},
                 id='step_except_one_run'),
    pytest.param({'except': ["VAR", "VAR2 == foo"]}, True,
                 {},
                 id='step_except_two_run'),
    pytest.param({'except': ["VAR"]}, False,
                 {"VAR": "VAL"},
                 id='step_except_one_skip'),
    pytest.param({'except': ["VAR", "VAR2 == foo"]}, False,
                 {"VAR": "VAL"},
                 id='step_except_two_skip'),
    pytest.param({'except': ["VAR2"],
                  'only': ["VAR"]}, True,
                 {"VAR": "VAL"},
                 id='step_only_and_except_run'),
    pytest.param({'except': ["VAR2"],
                  'only': ["VAR"]}, False,
                 {"VAR": "VAL", "VAR2": "foo"},
                 id='step_only_with_except_skip')
]


@pytest.mark.parametrize("step_data_opt, res, env", steps_data_only_except)
def test_step(step_data_opt, res, env, mocker, caplog):
    extra = extra_params('config')
    ini = getConfig('ini')
    ini['log']['deprecation_warning'] = False
    ini['env'] = EnvVars(load=False)
    ini['env'].update(env)
    mocker.patch('chainedci.step.Artifact', MockArtifact)
    step_data = {'stage': 'config',
                 'url': 'https://gitlab.me/project',
                 'branch': 'master',
                 'project': 'config'}
    step_data.update(step_data_opt)
    s = Step('step0', step_data, extra[0], 'fooSc')
    assert s.only_except_check() is res
    if not res:
        s.run()
        assert "Skip due to only/except reason" in caplog.text
    force_ini_values()


@pytest.mark.parametrize("step_data, artifact_result", [steps_pipeline[4]])
def test_step_timeout(step_data, artifact_result, caplog,
                      mocker):
    extra = extra_params('config')
    mocker.patch('chainedci.step.Pipeline', MockPipeline)
    mocker.patch('chainedci.step.getConfig', fake_getConfig)
    mocker.patch('chainedci.step.config', fake_config)
    s = Step('foo', step_data, extra[0], 'fooSc')
    s.artifact = None
    s.run()
    assert s.pipeline.timeout == 42
    assert ("set pipeline timeout to custom value"
            f" '{step_data['timeout']}'") in caplog.text

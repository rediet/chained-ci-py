#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

'''
Test ArtifactSrc.
'''

import mock
import shutil
import tempfile
import pytest
import schema
import requests
from chainedci.artifact_src import ArtifactSrc
from chainedci.config import config, getConfig, run
from chainedci.log import log
from tests.tests_lib import fake_token_selector, force_ini_values

force_ini_values()
config['defaults'] = {'gitlab': {'api_url': 'https://mygitlab'}}
log.setLevel('DEBUG')

REMOTE = [
    pytest.param(
        {
            'type': 'remote',
            'source': 'http://www.fake.url/data.yml',
            'destination': 'local_src.yml'},
        id="remote_file"),
    pytest.param(
        {
            'type': 'remote_archive',
            'source': 'http://www.fake.url/data.zip'},
        id="remote_archive")]
LOCAL = [
    pytest.param(
        {
            'type': 'local_archive',
            'source': 'tests/artifacts/artifact.zip'},
        id="local_archive"),
    pytest.param(
        {
            'type': 'local',
            'source': 'tests/artifacts/local_src.yml',
            'destination': 'local_src.yml'},
        id="local_file"),
    pytest.param(
        {
            'type': 'local',
            'source': 'tests/artifacts/local_src.yml',
            'destination': 'subfolder/local_src.yml'},
        id="local_file_with_subfolder"),
    pytest.param(
        {
            'type': 'local',
            'source': 'tests/artifacts/',
            'destination': ''},
        id="local_folder")]
PIPELINE = [
    pytest.param(
        {
            'type': 'pipeline',
            'source': 'config'},
        id="pipeline_simple"),
    pytest.param(
        {
            'type': 'pipeline',
            'source': 'config',
            'limit_to': [
                {'local_src.yml': 'new_file_name.yml'},
                {'local_src2.yml': 'new_file_name2.yml'}]},
        id="pipeline_with_limit"),
    pytest.param(
        {
            'type': 'pipeline',
            'source': 'config',
            'limit_to': [
                {'local_src.yml': 'new_file_name.yml'},
                {'local_src2.yml': 'subfolder/new_file_name2.yml'}]},
        id="pipeline_with_limit_and_subfolder")
]

ALL = REMOTE + LOCAL + PIPELINE


@pytest.mark.parametrize("artifact_src", ALL)
def test_artifact_init(artifact_src, requests_mock):
    src = ArtifactSrc(artifact_src)
    assert isinstance(src, dict) is True
    assert src == artifact_src


@pytest.mark.parametrize("artifact_src", [REMOTE[0]])
def test_remote_file_not_found(artifact_src, requests_mock, mocker):
    dest_folder = tempfile.mkdtemp()
    src = ArtifactSrc(artifact_src)
    mocked_file = 'tests/artifacts/local_src.yml'
    with open(mocked_file, mode='rb') as file:
        file_content = file.read()
        requests_mock.get(src['source'], content=file_content,
                          status_code=404)
        with pytest.raises(requests.exceptions.HTTPError):
            src.load(dest_folder)


def test_artifact_bad_type():
    with pytest.raises(schema.SchemaError):
        ArtifactSrc({'type': 'wrong value', 'source': 'url'})


def test_artifact_bad_key():
    with pytest.raises(schema.SchemaError):
        ArtifactSrc({'type': 'remote', 'foo': 'bar'})


def test_artifact_local_bad_dest():
    dest_folder = "/bad_folder"
    local_folder_artifact_src = {'type': 'local',
                                 'source': 'tests/artifacts/local_src.yml',
                                 'destination': 'local_src.yml'}
    artifact_source = ArtifactSrc(local_folder_artifact_src)
    with pytest.raises(OSError):
        artifact_source.load(dest_folder)


@pytest.mark.parametrize("artifact_src", PIPELINE)
def test_load_from_pipeline(artifact_src, mocker):
    src = ArtifactSrc(artifact_src)
    dest_folder = tempfile.mkdtemp()
    src._load_from_my_pipeline = mocker.MagicMock()
    src._load_from_pipeline(dest_folder)
    src._load_from_my_pipeline.assert_called_once_with(dest_folder)


@pytest.mark.parametrize("artifact_src", PIPELINE)
def test_load_from_pipeline_2(artifact_src, mocker):
    src = ArtifactSrc(artifact_src)
    src['artifact_in_pipeline'] = False
    dest_folder = tempfile.mkdtemp()
    src._load_from_another_pipeline = mocker.MagicMock()
    src._load_from_pipeline(dest_folder)
    src._load_from_another_pipeline.assert_called_once_with(dest_folder)


@mock.patch('chainedci.artifact_src.token_selector', fake_token_selector)
@pytest.mark.parametrize("artifact_src", [PIPELINE[0]])
def test_load_from_another_pipeline(artifact_src, requests_mock, caplog):
    src = ArtifactSrc(artifact_src)
    dest_folder = tempfile.mkdtemp()
    assert src._load_from_another_pipeline(dest_folder) is dest_folder


@mock.patch('chainedci.artifact_src.token_selector', fake_token_selector)
@pytest.mark.parametrize("artifact_src", [PIPELINE[0]])
def test_api_get_OK(artifact_src, requests_mock, caplog):
    src = ArtifactSrc(artifact_src)
    url = "http://gitlab.api"
    response = {'key': 'value'}
    requests_mock.get(url, json=response)
    res = src._api_get(url)
    assert res == response
    assert "call api" in caplog.text


@mock.patch('chainedci.artifact_src.token_selector', fake_token_selector)
@pytest.mark.parametrize("artifact_src", [PIPELINE[0]])
def test_api_get_KO(artifact_src, requests_mock, caplog):
    src = ArtifactSrc(artifact_src)
    url = "http://gitlab.api"
    response = {'error': '404'}
    requests_mock.get(url, json=response, status_code=404)
    with pytest.raises(requests.exceptions.HTTPError):
        src._api_get(url)
        assert "error calling " in caplog.text


@pytest.mark.parametrize("artifact_src", PIPELINE)
def test_load_from_my_pipeline(artifact_src, mocker, caplog):
    config['defaults'] = {'gitlab': {'api_url': 'https://mygitlab'}}
    src = ArtifactSrc(artifact_src)
    run_value = {'name': 'scenarioName'}
    mocker.patch('chainedci.artifact_src.getConfig', lambda x: run_value)
    destination = 'place/to/put/files'
    pipeline_data = [
        {'name': 'config:scenarioName', 'id': '123'},
        {'name': 'build:scenarioName', 'id': '456'}
    ]
    src._get_pipeline_successful_jobs = lambda: pipeline_data
    src._download_job_artifact = mocker.MagicMock()
    src._limit_to = mocker.MagicMock()
    mocker.patch('chainedci.artifact_src.mkdtemp', lambda: destination)
    src._load_from_pipeline(destination)
    src._download_job_artifact.assert_called_once_with('123', destination)
    if 'limit_to' in artifact_src:
        src._limit_to.assert_called_once_with(destination, destination)


@pytest.mark.parametrize("artifact_src", [PIPELINE[0]])
def test_load_from_my_pipeline_no_job(artifact_src, mocker, caplog):
    src = ArtifactSrc(artifact_src)
    destination = 'place/to/put/files'
    pipeline_data = [
        {'name': 'test:scenarioName', 'id': '123'},
        {'name': 'build:scenarioName', 'id': '456'}
    ]
    src._get_pipeline_successful_jobs = lambda: pipeline_data
    run_value = {'name': 'scenarioName'}
    mocker.patch('chainedci.artifact_src.getConfig', lambda x: run_value)
    with pytest.raises(ValueError):
        src._load_from_pipeline(destination)
        assert ("No job found with name "
                f"'{artifact_src['source']}'") in caplog.text


@pytest.mark.parametrize("artifact_src", [PIPELINE[0]])
def test_get_pipeline_successful_jobs(artifact_src, mocker, caplog):
    src = ArtifactSrc(artifact_src)
    ini = getConfig('ini')
    ini['env']['CI_PROJECT_ID'] = 666
    ini['env']['CI_PIPELINE_ID'] = 42
    src._api_get = mocker.MagicMock()
    src._get_pipeline_successful_jobs()
    awaited_url = ('https://mygitlab/projects/666/'
                   'pipelines/42/jobs?scope[]=success')
    src._api_get.assert_called_once_with(awaited_url)


@pytest.mark.parametrize("artifact_src", [PIPELINE[0]])
def test_download_job_artifact(artifact_src, mocker, caplog):
    src = ArtifactSrc(artifact_src)
    destination = 'place/to/put/files'
    ini = getConfig('ini')
    ini['env']['CI_PROJECT_ID'] = 666
    src.unpack = mocker.MagicMock()
    src._download_job_artifact(123, destination)
    assert src['source'] == ('https://mygitlab/projects/666/'
                             'jobs/123/artifacts')
    src.unpack.assert_called_once_with(destination)


@pytest.mark.parametrize("artifact_src", [PIPELINE[1]])
def test_limit_to(artifact_src, mocker, caplog):
    src = ArtifactSrc(artifact_src)
    tmp_folder = 'tmp/place/with/unpacked/files'
    destination = 'place/to/put/files'
    mocker.patch('chainedci.artifact_src.exists', lambda x: True)
    src._load_from_local = mocker.MagicMock()
    src._limit_to(tmp_folder, destination)


@pytest.mark.parametrize("artifact_src", [PIPELINE[1]])
def test_limit_to_file_not_in_artifact(artifact_src, mocker, caplog):
    src = ArtifactSrc(artifact_src)
    tmp_folder = 'tmp/place/with/unpacked/files'
    destination = 'place/to/put/files'
    mocker.patch('chainedci.artifact_src.exists', lambda x: False)
    with pytest.raises(ValueError):
        src._limit_to(tmp_folder, destination)


def mocked_load_from_pipeline(dest):
    shutil.copy("tests/artifacts/local_src.yml", f"{dest}/local_src.yml")


@mock.patch('chainedci.artifact_src.token_selector', fake_token_selector)
@pytest.mark.parametrize("artifact_src", ALL)
def test_load(artifact_src, requests_mock, mocker):
    dest_folder = tempfile.mkdtemp()
    src = ArtifactSrc(artifact_src)
    if artifact_src['type'] == 'pipeline':
        src._load_from_pipeline = mocked_load_from_pipeline
        src.load(dest_folder)
    elif artifact_src['type'] in ['local', 'local_archive']:
        src.load(dest_folder)
    else:
        mocked_file = ('tests/artifacts/local_src.yml'
                       if src['type'] == 'remote'
                       else 'tests/artifacts/artifact.zip')
        with open(mocked_file, mode='rb') as file:
            file_content = file.read()
            requests_mock.get(src['source'], content=file_content)
            src.load(dest_folder)
    check_file = 'local_src.yml'
    if 'destination' in artifact_src:
        if artifact_src['destination'] != '':
            check_file = artifact_src['destination']
    check_file = dest_folder + '/' + check_file
    with open(check_file, 'r') as dest_file:
        assert dest_file.read() == '---\nfake:\n  foo: bar\n'
    shutil.rmtree(dest_folder)


@pytest.mark.parametrize("artifact_src", [LOCAL[0]])
def test_load_to_nowhere(artifact_src):
    src = ArtifactSrc(artifact_src)
    with pytest.raises(ValueError):
        src.load(None)


def test_load_bad_archive():
    artifact_src = {
        'type': 'local_archive',
        'source': 'tests/artifacts/artifact_bad.zip'}
    src = ArtifactSrc(artifact_src)
    with pytest.raises(ValueError):
        src.load('.')

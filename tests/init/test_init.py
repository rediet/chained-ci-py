#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import glob
import tempfile
from chainedci.init_project import (
    init_project, create_all_file, create_folders, create_static_files)
from chainedci.config import config, getConfig


class FakeOrigin():
    def __init__(self):
        self.url = 'git@gitlab.com:mygroup/myproject.git'


class FakeRepo():
    def __init__(self, folder):
        self.remotes = {'origin': FakeOrigin()}


def test_create_folders(mocker):
    mocker.patch('chainedci.init_project.git.Repo', FakeRepo)
    cf_folder = tempfile.mkdtemp()
    sc_folder = tempfile.mkdtemp()
    ini = getConfig('ini')
    ini['init']['tree']['scenarios']['folder'] = sc_folder
    ini['init']['tree']['config'] = cf_folder
    init_project()
    cf_tree = sorted(glob.glob(cf_folder+'**/**/*', recursive=True))
    assert cf_tree == [
        cf_folder + '/certificates',
        cf_folder + '/config',
        cf_folder + '/config/artifacts',
        cf_folder + '/config/projectA_config1.yml',
        cf_folder + '/config/projectA_config2.yml',
        cf_folder + '/config/ssh_gateways',
        cf_folder + '/ssh_creds'
    ]
    sc_tree = sorted(glob.glob(sc_folder+'**/**/*', recursive=True))
    assert sc_tree == [
        sc_folder + '/group_vars',
        sc_folder + '/group_vars/all.yml',
        sc_folder + '/host_vars',
        sc_folder + '/host_vars/projectA.yml',
        sc_folder + '/inventory'
    ]
    with open(sc_folder + '/group_vars/all.yml', mode='r') as all_file:
        all_file_content = all_file.read()
    all_ref_content = ''
    with open('tests/init/all_reference.yml', mode='r') as all_ref:
        for line in all_ref.readlines():
            all_ref_content += line.replace('__CONFIGFOLDER__', cf_folder)
    assert all_file_content == all_ref_content
